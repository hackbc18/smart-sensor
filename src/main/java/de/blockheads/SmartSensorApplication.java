package de.blockheads;

import com.tinkerforge.IPConnection;
import de.blockheads.driver.BarometerDriver;
import de.blockheads.driver.DisplayDriver;
import de.blockheads.mdl.LedgerConnector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import javax.json.JsonObject;

import static javax.json.Json.createObjectBuilder;

@SpringBootApplication
@EnableScheduling
public class SmartSensorApplication {
    private static final Logger log = LoggerFactory.getLogger(SmartSensorApplication.class);

    @Value("${con.host}")
    private String host;

    @Value("${con.port}")
    private Integer port;

    @Bean
    public IPConnection getIpConnection() throws Exception {
        IPConnection connection = new IPConnection();
        connection.connect(host, port);
        return connection;
    }

    public static void main(String[] args) {
        SpringApplication.run(SmartSensorApplication.class, args);
    }
}
