package de.blockheads.mdl;


import com.fasterxml.jackson.annotation.JsonRawValue;

import javax.json.JsonObject;

public class SensorPayload {
    private String address;
    private String prevHash;
    @JsonRawValue
    private JsonObject data;

    public SensorPayload(String address, String prevHash, JsonObject data) {
        this.address = address;
        this.prevHash = prevHash;
        this.data = data;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPrevHash() {
        return prevHash;
    }

    public void setPrevHash(String prevHash) {
        this.prevHash = prevHash;
    }

    public JsonObject getData() {
        return data;
    }

    public void setData(JsonObject data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "SensorPayload{" +
                "address='" + address + '\'' +
                ", prevHash='" + prevHash + '\'' +
                ", data=" + data +
                '}';
    }
}
