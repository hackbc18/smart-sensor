package de.blockheads.driver;

import com.tinkerforge.BrickletLCD20x4;
import com.tinkerforge.IPConnection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

@Component
public class DisplayDriver {
    private static final Logger log = LoggerFactory.getLogger(DisplayDriver.class);

    private BrickletLCD20x4 bricklet;

    @Value("${sensors.display.id}")
    private String brickletId;

    @Value("${iota.enabled}")
    private Boolean iotaEnabled;

    @Value("${ethereum.enabled}")
    private Boolean ethereumEnabled;

    @Autowired
    private IPConnection connection;

    @PostConstruct
    private void setUp() throws Exception {
        log.debug("Initializing Barometer Bricklet: {}", brickletId);
        bricklet = new BrickletLCD20x4(brickletId, connection);
        bricklet.backlightOn();
        bricklet.clearDisplay();
        bricklet.writeLine((byte)0, (byte) 0, iotaEnabled ? "IOTA" : ethereumEnabled ? "Ethereum" : "NO CON");
    }

    @PreDestroy
    private void tearDown() throws Exception {
        bricklet.clearDisplay();
        bricklet.backlightOff();
    }

    public void printAddress(String address) {
        try {
            bricklet.writeLine((byte) 1, (byte) 0, "Addr: " + address.substring(0,14));
        } catch (Exception e) {
            log.error("Can't print address {}", address, e);
        }
    }

    public void printLastHash(String hash) {
        try {
            bricklet.writeLine((byte) 2, (byte) 0, "Hash: " + hash.substring(0,14));
        } catch (Exception e) {
            log.error("Can't print hash {}", hash, e);
        }
    }

    public void printTemperature(Double temperature) {
        try {
            bricklet.writeLine((byte) 3, (byte) 0, "Temp: " + String.format("%4.2f" , temperature));
        } catch (Exception e) {
            log.error("Can't print temperature {}", temperature, e);
        }
    }
}
