package de.blockheads.mdl;

import de.blockheads.RestRequestLogger;
import de.blockheads.driver.DisplayDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import javax.json.Json;
import javax.json.JsonObject;
import java.io.StringReader;
import java.util.Collections;

@Component
@ConditionalOnExpression("${iota.enabled}")
public class IotaConnector implements LedgerConnector {

    private static final Logger log = LoggerFactory.getLogger(IotaConnector.class);

    @Autowired
    private DisplayDriver display;

    @Value("${iota.web-service.host}:${iota.web-service.port}")
    private String wsHost;

    @Value("${iota.address}")
    private String address;

    private String prevHash = "";

    @PostConstruct
    private void setUp() {
        log.debug("Using address: {}", address);
        display.printAddress(address);
    }

    @Override
    public void push(JsonObject data) {
        RestTemplate restTemplate = new RestTemplate();
        //restTemplate.setInterceptors(Collections.singletonList(new RestRequestLogger()));
        SensorPayload sensorPayload = new SensorPayload(address, prevHash, data);

        log.debug("Pushing Data: {}", sensorPayload);
        String pushResult = restTemplate.postForObject(wsHost + "/api/push", sensorPayload, String.class);
        log.debug("Raw WebService result: {}", pushResult);
        JsonObject pushResJson = Json.createReader(new StringReader(pushResult)).readArray().getJsonObject(0);

        prevHash = pushResJson.getString("hash");
        display.printLastHash(prevHash);
    }
}
