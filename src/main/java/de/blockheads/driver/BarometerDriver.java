package de.blockheads.driver;

import com.tinkerforge.BrickletBarometer;
import com.tinkerforge.IPConnection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class BarometerDriver {
    private static final Logger log = LoggerFactory.getLogger(BarometerDriver.class);

    private BrickletBarometer bricklet;

    @Value("${sensors.barometer.id}")
    private String brickletId;

    @Autowired
    private IPConnection connection;

    @PostConstruct
    private void setUp() {
        log.debug("Initializing Barometer Bricklet: {}", brickletId);
        bricklet = new BrickletBarometer(brickletId, connection);
    }

    public BrickletBarometer bricklet() {
        return bricklet;
    }
}
