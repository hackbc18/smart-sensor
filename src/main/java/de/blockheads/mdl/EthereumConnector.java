package de.blockheads.mdl;

import de.blockheads.driver.DisplayDriver;
import de.blockheads.mdl.smartcontracts.SensorData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.web3j.abi.EventEncoder;
import org.web3j.abi.TypeReference;
import org.web3j.abi.datatypes.Address;
import org.web3j.abi.datatypes.Event;
import org.web3j.abi.datatypes.Utf8String;
import org.web3j.abi.datatypes.generated.Uint256;
import org.web3j.crypto.Credentials;
import org.web3j.crypto.WalletUtils;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.DefaultBlockParameterNumber;
import org.web3j.protocol.core.methods.request.EthFilter;
import org.web3j.protocol.core.methods.response.Log;
import org.web3j.protocol.core.methods.response.Web3ClientVersion;
import org.web3j.protocol.http.HttpService;
import rx.functions.Func1;

import javax.annotation.PostConstruct;
import javax.json.JsonObject;
import java.util.Arrays;

import static java.math.BigInteger.valueOf;

@Component
@ConditionalOnExpression("${ethereum.enabled}")
public class EthereumConnector implements LedgerConnector {
    private static final Logger log = LoggerFactory.getLogger(IotaConnector.class);

    @Value("${ethereum.address}")
    private String address;

    @Value("${ethereum.client.url}")
    private String clientUrl;

    @Value("${ethereum.client.password}")
    private String password;

    @Value("${ethereum.client.wallet-path}")
    private String walletPath;

    @Autowired
    private DisplayDriver display;
    private Web3j web3j;
    private SensorData sensorContract;

    @PostConstruct
    private void setUp() throws Exception {
        log.debug("Using address: {}", address);
        log.debug("Ethereum client URL: {}", clientUrl);
        web3j = Web3j.build(new HttpService(clientUrl));
        Web3ClientVersion web3ClientVersion = web3j.web3ClientVersion().send();
        log.debug("Web3Client Version: {}", web3ClientVersion.getWeb3ClientVersion());
        Credentials credentials = WalletUtils.loadCredentials(password, walletPath);
        sensorContract = SensorData.load(address, web3j, credentials, valueOf(10000L), valueOf(1000000L));
        String sensorId = sensorContract.getSensorId().send();
        log.debug("Sensor Id: {}, address: {}", sensorId, address);
        display.printAddress(address);
    }

    @Override
    public void push(JsonObject data) throws Exception {
        log.debug("Pushing {} to Ethereum", data);
        int temperature = (int) (data.getJsonNumber("temperature").doubleValue() * 100.0);
        String hash = sensorContract.setTemperature(valueOf(temperature)).send().getBlockHash();
        log.debug("Transcation hash: {}", hash);
        display.printLastHash(hash);
    }

    //@Scheduled(fixedDelay = 30000)
    public void checkHistory() {
        final Event event = new Event("SensorDataEvent",
                Arrays.<TypeReference<?>>asList(),
                Arrays.<TypeReference<?>>asList(new TypeReference<Address>() {
                }, new TypeReference<Utf8String>() {
                }, new TypeReference<Uint256>() {
                }));
        EthFilter filter = new EthFilter(new DefaultBlockParameterNumber(0), new DefaultBlockParameterNumber(200), address);
        filter.addSingleTopic(EventEncoder.encode(event));
        web3j.ethLogObservable(filter).map((Func1<Log, SensorData.SensorDataEventEventResponse>) log -> {
            System.out.println(log);
            return null;
        });
    }
}
