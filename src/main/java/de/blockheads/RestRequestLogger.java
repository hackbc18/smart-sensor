package de.blockheads;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;

import java.io.IOException;
import java.nio.charset.Charset;

/**
 * Created by Martin on 04.02.2018.
 */
public class RestRequestLogger implements ClientHttpRequestInterceptor {
    private static final Logger log = LoggerFactory.getLogger(RestRequestLogger.class);

    @Override
    public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution) throws IOException {
        ClientHttpResponse response = execution.execute(request, body);

        log.debug("Request: \n\tmethod: {}, \n\tURI: {}, \n\theaders: {}, \n\tbody: {}, \n\tstatus code: {}, \n\tresponse headers: {}, \n\tresponse body: {}",
                request.getMethod(),
                request.getURI(),
                request.getHeaders(),
                new String(body, Charset.forName("UTF-8")),
                response.getStatusCode(),
                response.getHeaders(),
                new String(IOUtils.toByteArray(response.getBody()), Charset.forName("UTF-8")));

        return response;
    }
}
