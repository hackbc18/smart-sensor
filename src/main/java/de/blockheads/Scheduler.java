package de.blockheads;

import de.blockheads.driver.BarometerDriver;
import de.blockheads.driver.DisplayDriver;
import de.blockheads.mdl.LedgerConnector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.json.JsonObject;

import static javax.json.Json.createObjectBuilder;

/**
 * Created by Martin on 03.02.2018.
 */
@Component
public class Scheduler {
    private static final Logger log = LoggerFactory.getLogger(Scheduler.class);

    @Autowired
    BarometerDriver barometer;

    @Autowired
    private DisplayDriver display;

    @Autowired
    private LedgerConnector connector;

    @Scheduled(fixedDelay = 10000)
    private void updateLedger() throws Exception {
        log.debug("Updating ledger");
        Double temp = barometer.bricklet().getChipTemperature() / 100.0;
        display.printTemperature(temp);
        log.debug("Temperature: {}", temp);
        JsonObject payload = createObjectBuilder().add("temperature", temp).build();
        connector.push(payload);
    }
}
