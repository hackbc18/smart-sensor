package de.blockheads.mdl;

import javax.json.JsonObject;

/**
 * Created by Martin on 03.02.2018.
 */
public interface LedgerConnector {
    void push(JsonObject data) throws Exception;
}
