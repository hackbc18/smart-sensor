pragma solidity ^0.4.0;

contract SensorData {

    uint public temperature;
    string public sensorId;

    event SensorDataEvent(address from, string sensorId, uint temperature);

    /**
     * Constructor
     */
    function SensorData(string _sensorId) public {
        sensorId = _sensorId;
    }

    function setTemperature(uint _temperature) public {
        temperature = _temperature;
        SensorDataEvent(msg.sender, sensorId, _temperature);
    }

    /**
     * Get the temperature.
     */
    function getTemperature() constant public returns (uint) {
        return temperature;
    }

    /**
     * Get the sensor id.
     */
    function getSensorId() constant public returns (string) {
        return sensorId;
    }

}